import random

import pygame
import settings


class MagicBullet(pygame.sprite.Sprite):
    def __init__(self, pos, groups, dir_x, dir_y, enemies_sprites):
        super().__init__(groups)
        self.enemies_sprites = enemies_sprites
        image = pygame.image.load('graphics/player/bullet.png')
        self.image = pygame.transform.scale(image, (40,40))
        self.rect = self.image.get_rect(topleft=pos)
        self.directions = pygame.math.Vector2()
        self.vel = 4
        self.dir_x = dir_x
        self.dir_y = dir_y
        self.handle_dir()

    def handle_dir(self):
        if self.dir_x == 0 and self.dir_y == 0:
            self.dir_x = 0
            self.dir_y = -1

    def update(self):
        self.rect.x += self.vel * self.dir_x
        self.rect.y += self.vel * self.dir_y
        self.collisions()
        if self.rect.x > settings.WIDTH or self.rect.x < 0:
            self.destroy_bullet()

    def destroy_bullet(self):
        self.remove(self.groups())

    def collisions(self):
        for sprite in self.enemies_sprites:
            if sprite.rect.colliderect(self.rect):
                sprite.destroy_meteor()
