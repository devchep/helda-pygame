import pygame


def print_message(*msg):
    win = pygame.display.get_surface()
    font = pygame.font.Font(None, 60)
    msg_surface = font.render(str(msg), True, 'White')
    rect_surface = msg_surface.get_rect(topleft=(10, 20))
    pygame.draw.rect(win, 'Black', rect_surface)
    win.blit(msg_surface, rect_surface)