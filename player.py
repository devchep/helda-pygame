import os

import pygame

from magic_bullet import MagicBullet
from message import print_message
from settings import OBJ_SIZE, PLAYER_SIZE


class Player(pygame.sprite.Sprite):
    def __init__(self,
                 pos,
                 groups,
                 rock_sprites,
                 enemies_sprites,
                 game_over,
                 clear_meteor_counter):
        super().__init__(groups)
        self.groups = groups
        image = pygame.image.load('graphics/player/down/down.png')
        self.sounds = {'get_hurt': pygame.mixer.Sound('sounds/minecraft_hurt.mp3')}
        self.image = pygame.transform.scale(image, (OBJ_SIZE, OBJ_SIZE))
        self.rect = self.image.get_rect(topleft=pos)
        # Перемещение
        self.dir_x = 0
        self.dir_y = 0
        self.vel = 6
        self.status = 'down'
        self.magic_attacking = False
        self.magic_attack_time = 300
        self.current_attack_time = 0
        # Группы
        self.rock_sprites = rock_sprites
        self.enemies_sprites = enemies_sprites
        self.animations = {'up':[],
                           'down':[],
                           'left':[],
                           'right':[],
                           'magic':[]
                           }
        self.frame_speed = 0.10
        self.frame = 0
        self.load_animations()
        # Вспомогательное
        self.game_over = game_over
        self.clear_meteor_counter = clear_meteor_counter

    def load_animations(self):
        path = 'graphics/player/'
        for animation in self.animations:
            full_path = path + animation
            for pic in os.listdir(full_path):
                pic_path = full_path+'/'+pic
                image_surface = pygame.image.load(pic_path)
                image_surface = pygame.transform.scale(image_surface,
                                                       (PLAYER_SIZE,
                                                        PLAYER_SIZE))
                self.animations[animation].append(image_surface)
        print(self.animations)

    def set_status(self):
        if self.magic_attacking:
            self.status = 'magic'
            return
        if self.dir_x == 0 and self.dir_y == 0:
            self.status = 'down'

    def cooldown(self):
        current_time = pygame.time.get_ticks()
        if current_time - self.current_attack_time >= self.magic_attack_time:
            self.magic_attacking = False


    def animate(self):
        frames = self.animations[self.status]

        self.frame += self.frame_speed
        self.frame %= len(frames)
        self.image = frames[int(self.frame)]
        self.rect = self.image.get_rect(center=self.rect.center)

    def magic(self):
        if not self.magic_attacking:
            MagicBullet(self.rect.center, self.groups,
                        self.dir_x,
                        self.dir_y,
                        self.enemies_sprites
                        )
        self.magic_attacking = True
        self.current_attack_time = pygame.time.get_ticks()

    def input(self):
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[pygame.K_k]:  # магия
            self.magic()

        if pressed_keys[pygame.K_w]:  # движение вверх
            self.dir_y = -1
            self.status = 'up'
        elif pressed_keys[pygame.K_s]:  # движение вниз
            self.dir_y = 1
            self.status = 'down'
        else:
            self.dir_y = 0

        if pressed_keys[pygame.K_a]:  # движение влево
            self.dir_x = -1
            self.status = 'left'
        elif pressed_keys[pygame.K_d]:  # движение вправо
            self.dir_x = 1
            self.status = 'right'
        else:
            self.dir_x = 0

    def move(self):
        self.input()
        self.rect.x += self.dir_x * self.vel
        self.collision('x')
        self.rect.y += self.dir_y * self.vel
        self.collision('y')

    def update(self):
        self.move()
        self.set_status()
        self.animate()
        self.cooldown()

    def collision(self, direction):
        for sprite in self.rock_sprites:
            if sprite.rect.colliderect(self.rect):
                if direction == 'x':
                    if self.dir_x > 0:
                        self.rect.right = sprite.rect.left
                    if self.dir_x < 0:
                        self.rect.left = sprite.rect.right
                if direction == 'y':
                    if self.dir_y > 0:
                        self.rect.bottom = sprite.rect.top
                    if self.dir_y < 0:
                        self.rect.top = sprite.rect.bottom

        for sprite in self.enemies_sprites:
            if sprite.rect.colliderect(self.rect):
                self.sounds['get_hurt'].play()
                sprite.destroy_meteor()
                self.game_over()
                self.clear_meteor_counter()
