from random import randint

import pygame

from message import print_message
from meteor import Meteor
from player import Player
from rock import Rock
from settings import *


class Level:
    def __init__(self, game_over):
        self.visible_sprites = pygame.sprite.Group()
        self.rock_sprites = pygame.sprite.Group()
        self.enemies_sprites = pygame.sprite.Group()
        self.win = pygame.display.get_surface()
        self.game_over = game_over
        self.draw_map()
        self.meteor_counter = 0

    def draw_map(self):
        for row_index, row in enumerate(LEVEL_MAP):
            for col_index, val in enumerate(row):
                if val == 'x':
                    Rock((col_index*OBJ_SIZE, row_index*OBJ_SIZE),
                         [self.visible_sprites, self.rock_sprites])
                if val == 'p':
                    Player((col_index*OBJ_SIZE, row_index*OBJ_SIZE),
                           [self.visible_sprites],
                           self.rock_sprites,
                           self.enemies_sprites,
                           self.game_over,
                           self.clear_meteor_counter)

    def update_meteor_counter(self):
        self.meteor_counter += 1

    def clear_meteor_counter(self):
        self.meteor_counter = 0

    def get_meteor_counter(self):
        return self.meteor_counter

    def gen_meteor(self):
        x = randint(0, 1024)
        Meteor((x, 0), [self.visible_sprites, self.enemies_sprites], self.update_meteor_counter)

    def run(self):
        self.visible_sprites.draw(self.win)
        self.visible_sprites.update()
        # print_message("Score", self.meteor_counter)
