import random

import pygame
import settings


class Meteor(pygame.sprite.Sprite):
    def __init__(self, pos, groups, update_counter):
        super().__init__(groups)
        image = pygame.image.load('graphics/enemy/enemy.png')
        self.image = pygame.transform.scale(image, (settings.PLAYER_SIZE,
                                                    settings.PLAYER_SIZE))
        self.rect = self.image.get_rect(topleft=pos)
        self.directions = pygame.math.Vector2()
        self.vel_x = 5
        self.vel_y = 5
        self.dir_x = 1
        self.dir_y = 1
        self.choose_dir()
        self.choose_vel()
        self.update_counter = update_counter

    def choose_vel(self):
        self.vel_x = random.randint(3,5)
        self.vel_y = random.randint(3, 5)

    def choose_dir(self):
        direction = random.randint(1, 2)
        self.dir_x = 1 if direction % 2 == 0 else -1

    def update(self):
        self.rect.x += self.vel_x * self.dir_x
        self.rect.y += self.vel_y * self.dir_y
        if self.rect.x > settings.WIDTH or self.rect.x < 0:
            self.destroy_meteor()
            self.update_counter()

    def destroy_meteor(self):
        self.remove(self.groups())
