import pygame
from settings import OBJ_SIZE


class Rock(pygame.sprite.Sprite):
    def __init__(self, pos, groups):
        super().__init__(groups)
        image = pygame.image.load('graphics/obstacle.png')
        self.image = pygame.transform.scale(image, (OBJ_SIZE, OBJ_SIZE))
        self.rect = self.image.get_rect(topleft=pos)
