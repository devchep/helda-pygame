import pygame
import sys

from message import print_message
from settings import *
from level import Level


class Main:
    def __init__(self):
        pygame.init()
        self.main_win = pygame.display.set_mode((WIDTH, HEIGHT))
        self.FPS = FPS
        self.clock = pygame.time.Clock()
        self.level = Level(self.game_over)
        self.game_running = True
        self.meteor_event = pygame.USEREVENT + 0

    def game_over(self):
        self.game_running = False

    def game(self):
        self.main_win.fill('White')
        self.level.run()

    def menu(self):
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[pygame.K_r]:
            self.game_running = True
            return
        self.main_win.fill('White')
        print_message('Game Over! Press R to restart!')

    def run(self):

        pygame.time.set_timer(self.meteor_event, 1000)

        while True:
            if self.game_running:
                self.game()
            else:
                self.menu()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == self.meteor_event:
                    self.level.gen_meteor()

            pygame.display.update()
            self.clock.tick(self.FPS)


if __name__ == '__main__':
    main = Main()
    main.run()